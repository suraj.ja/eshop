class APIFeatures {
    constructor(query,queryString){
        this.query = query;
        this.queryString = queryString;
        // console.log(this.queryString);
       
    }
    filter(){
        
        let queryObj = {...this.queryString};
        let excludeArray = ['page','sort','limit','fields'];
        excludeArray.forEach(element => {
            delete queryObj[element]
        });
        let queryStr = JSON.stringify(queryObj);
        // queryString = this.query.find(JSON.stringify(queryString));
        queryStr = queryStr.replace(/\b(gte|gt|lte|lt)\b/g, match => `$${match}`);
        this.query = this.query.find(JSON.parse(queryStr));
        return this;
    }
    sort(){
        // console.log(typeof this.queryString)
        const sortParam = this.queryString ? this.queryString.sort : undefined
        
        if(this.queryString && sortParam){
            this.query = this.query.sort(sortParam);
        }else{
            this.query = this.query.sort('-createdAt');
        }
        return this;
    }
    // limitFields() {
    //     // if (this.queryString.fields) {
    //     //   const fields = this.queryString.fields.split(',').join(' ');
    //     //   this.query = this.query.select('name');
    //     // } else {
    //     //   this.query = this.query.select('-__v');
    //     // }
    //     this.query = this.query.select({ name: 1})
    //     return this;
    //   }
    paginate(){
        const skip = this.queryString ? this.queryString.pageNo ? (this.queryString.pageNo - 1) : 0 :0;
        const limit = this.queryString ? this.queryString.limit ? this.queryString.limit : 5:0;
        // console.log(pageNo, limit)
        this.query = this.query.skip(limit * skip).limit(limit)
        return this;
    }
}

module.exports = APIFeatures;