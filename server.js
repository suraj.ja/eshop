const dotenv = require('dotenv');
const app = require('./app')


process.on('uncaughtException',(error)=>{
    console.log('error=>', console.log(error))
    process.exit(1);
})
dotenv.config({path:'./config.env'});
const PORT = process.env.PORT || 4000;

const server = app.listen(PORT,()=>{
    // console.log(`server running on the port ${PORT}`);
})

process.on('unhandledRejection',(error)=>{
    // console.log(error);
    server.close(()=>{
        process.exit(1);
    })
})