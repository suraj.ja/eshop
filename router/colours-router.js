const express = require('express');
const colourController = require('../controllers/colours-controller');
const authController = require('../controllers/auth-controller')
const router = express.Router();

router.route('/:id')
.get(authController.authentication,authController.getId,colourController.getColor)
.patch(authController.authentication,authController.getId,colourController.updateColour)
.delete(authController.authentication,authController.getId,colourController.deleteColour)
router.route('/')
.get(authController.authentication,colourController.getAllColours)
.post(authController.authentication,authController.authorization('admin'),colourController.createcolour)

module.exports = router