const express = require('express');
const router = express.Router();
const authController = require('../controllers/auth-controller');
const ratingController = require('../controllers/ratings.controller')
router.route('/:id').get(authController.authentication,authController.getId,ratingController.getOneRatings)
router.route('/').post(authController.authentication,ratingController.createRating).get(authController.authentication,ratingController.getAllratings)

module.exports = router