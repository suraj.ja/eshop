const express = require('express');
const categoryController = require('../controllers/category-controller');
const authController = require('../controllers/auth-controller')
const router = express.Router();

router.route('/:id')
.get(authController.authentication,authController.getId,categoryController.getCateory)
.patch(authController.authentication,authController.getId,categoryController.updateCateory)
.delete(authController.authentication,authController.getId,categoryController.deleteCategory)
router.route('/')
.get(authController.authentication,categoryController.getAllCategory)
.post(authController.authentication,authController.authorization('admin'),categoryController.createCategory)

module.exports = router