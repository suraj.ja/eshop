const express = require('express');
const authController = require('../controllers/auth-controller');
const productController = require('../controllers/product-controller')
const router = express.Router();

router.route('/:id')
.get(authController.authentication,authController.getId,productController.getProduct)
.patch(authController.authentication,authController.getId,productController.updateProduct)
.delete(authController.authentication,authController.getId,productController.deleteProduct)
router.route('/')
.get(authController.authentication, productController.getAllProduct)
.post(authController.authentication,productController.createProduct)

module.exports = router