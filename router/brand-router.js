const express = require('express');
const brandController = require('../controllers/brand-controller');
const authController = require('../controllers/auth-controller')
const router = express.Router();

router.route('/:id')
.get(authController.authentication,authController.getId,brandController.getBrand)
.patch(authController.authentication,authController.getId,brandController.updateBrand)
.delete(authController.authentication,authController.getId,brandController.deleteBrand)
router.route('/')
.get(authController.authentication,brandController.getAllBrands)
.post(authController.authentication,authController.authorization('admin'),brandController.createBrand)

module.exports = router