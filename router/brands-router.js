const express = require('express');
const brandController = require('../controllers/brand-controller');
const authController = require('../controllers/auth-controller')
const router = express.Router();

router.route('/:id')
.get(authController.authentication,authController.getId,brandController.getCateory)
.patch(authController.authentication,authController.getId,brandController.updateCateory)
.delete(authController.authentication,authController.getId,brandController.deleteCategory)
router.route('/')
.get(authController.authentication,brandController.getAllCategory)
.post(authController.authentication,authController.authorization('admin'),brandController.createCategory)

module.exports = router