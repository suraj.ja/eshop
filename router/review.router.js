const express = require('express');
const router = express.Router();
const authController = require('../controllers/auth-controller');
const reviewController = require('../controllers/reviews.controller')
router.route('/:id').get(authController.authentication,authController.getId, reviewController.getOneReview)
router.route('/').get(authController.authentication, reviewController.getAllReviews).post(authController.authentication, reviewController.createReview)

module.exports = router