const express = require('express');
const app = require('../app');
const router = express.Router();
const authController = require('../controllers/auth-controller');
const userController = require('../controllers/user-controller');
router.route('/signup').post(authController.signup)
router.route('/login').post(authController.login)

router.use(authController.authentication)
// router.use(authController.authorization('admin'))
router.route('/:id').get(authController.getId,userController.getUser).delete(authController.getId, userController.deleteuser).patch(authController.getId,userController.updateUser);
router.route('/').get(authController.authorization('admin'), userController.getUsers).patch(userController.updateAll)

module.exports = router