const mongoose = require("mongoose");

const reviewSchema = new mongoose.Schema(
  {
    users: {
      type: mongoose.Schema.ObjectId,
      ref: "users",
      required: [true, "Review must belong to a user"],
    },
    createdAt: {
      type: Date,
      default: Date.now(),
    },
    rating: {
      type: Number,
      min: [1, "minimum rating should be 1"],
      max: [5, "maximum rating should be 5"],
    },
    review: {
      type: String,
      minlength: [2, "minimum length is 2"],
      maxlength: [30, "minimum length is 30"],
    },
    products: {
      type: mongoose.Schema.ObjectId,
      ref: "products",
    },
    active:{
      type:Boolean,
      default:true
    }
  },

  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
    versionKey: false,
  }
);

const reviewModel = mongoose.model("reviews", reviewSchema);

module.exports = reviewModel;
