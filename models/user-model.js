const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const validators = require("validator");

const userSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "name is required"],
    },
    email: {
      type: String,
      required: [true, "name is required"],
      unique: [true, "email must be unique"],
      validate: {
        validators: function (el) {
          return validators.isEmail(el);
        },
      },
    },
    email: {
      type: String,
      required: [true, "email is required"],
      unique: [true, "email must be unique"],
    },
    role: {
      type: String,
      default: "user",
      enum: ["user", "admin"],
    },
    password: {
      type: String,
      required: [true, "password is required"],
      select: false,
    },
    passwordConfirm: {
      type: String,
      required: [true, "passwordConfirm is required"],
      validate: {
        validator: function (v) {
          return this.password === v;
        },
        message: (props) => `password does not match`,
      },
    },
    passwordChangeAt: Date,
    resetPasswordToken: String,
    passwordResetExpired: Date,
    createdAt: {
      type: Date,
      default: Date.now(),
    },
    active: {
      type: Boolean,
      default: true,
      select: false,
    },
  },
  {
    versionKey: false,
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

userSchema.pre("save", async function (next) {
  if (!this.isModified("password")) {
    return next();
  }
  this.password = await bcrypt.hash(this.passwordConfirm, 12);
  this.passwordConfirm = undefined;
  this.passwordChangedAt = Date.now() - 1000;
  next();
});
userSchema.pre(/^find/, function (next) {
  this.find({ active: { $ne: false } });
  this.select("-active -__v");
  next();
});

userSchema.methods.correctPassword = async (candidatePassword, userPassword) =>
  await bcrypt.compare(candidatePassword, userPassword);

const userModel = mongoose.model("users", userSchema);

module.exports = userModel;
