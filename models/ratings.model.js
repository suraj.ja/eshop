const { text } = require('express');
const mongoose = require('mongoose');

const ratingSchema = new mongoose.Schema({
    review: {
        type: String,
        required: [true, 'review is required']
    },
    users: {
        type: mongoose.Schema.ObjectId,
        ref: 'users'
    },
    rating: {
        type: Number,
        min: [1, "minimum rating should be 1"],
        max: [5, "maximum rating should be 5"],
    },
    products:{
        type: mongoose.Schema.ObjectId,
        ref:'products',
        required:[true,'product is required']
    },
    active:{
      type:Boolean,
      default:true
    }
}, {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
    versionKey: false,
})

const ratingModel = new mongoose.model('ratings', ratingSchema)

module.exports = ratingModel