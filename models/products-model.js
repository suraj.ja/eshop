const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "name is required"],
  },
  price: {
    type: Number,
    min: [0, "should be greater tahn 0"],
    required: [true, "name is required"],
  },
  ratingQuntity: {
    type: mongoose.Schema.ObjectId,
    ref: "reviews",
  },
  ratingAvg: {
    type: mongoose.Schema.ObjectId,
    ref: "reviews",
  },
  discount: {
    type: Number,
    default: 0,
  },
  discountMaxAmt: {
    type: Number,
    required: [
      function () {
        if (this.discount > 0) {
          return true;
        } else {
          return false;
        }
      },
      "discount should added to set max amount",
    ],
  },
  actulPrice:{
    type : Number,
    default: function(el){
      // console.log(this.price * this.discount /100 , this.discountMaxAmt)
      var discount = this.discountMaxAmt? this.price * (this.discount /100) <= this.discountMaxAmt ? this.price - (this.price * this.discount /100) : this.price - this.discountMaxAmt:this.price;
      return discount;
    }
  },
  category: [
    {
      type: mongoose.Schema.ObjectId,
      required: [true, "category is required"],
      ref: "categories",
    },
  ],
  colours: [
    {
      type: mongoose.Schema.ObjectId,
      ref: "colours",
    },
  ],
  brand: {
    type: mongoose.Schema.ObjectId,
    ref: "brand",
    required: [true, "brand is reuqired"],
  },
  active:{
    type:Boolean,
    default:true,
    select:false
  },
  createdAt:{
    type:Date,
    default:Date.now()
  }
  //   SpecialOffer: [
  //     {
  //       type: mongoose.Schema.ObjectId,
  //       ref: "offers",
  //     },
  //   ],
},{
  versionKey:false,
  toJSON: { virtuals: true },
  toObject: { virtuals: true }
});

productSchema.methods.getActualPrice = function getActualPrice(){
  console.log(this.price)
  return 0;
}
productSchema.virtual('actualPrice').get(function (){
  return this.price
})

productSchema.post(/find/, ()=>{
  
})

const productModel = mongoose.model("products", productSchema);

module.exports = productModel;
