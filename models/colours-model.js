const mongoose = require('mongoose');

const colourSchema = new mongoose.Schema({
    name:{
        type:String,
        unique:[true, 'category must be unique']
    },
    createdAt:{
        type:Date,
        default:Date.now()
    },
    active:{
        type:Boolean,
        default:true,
        select:false
    }
},
{ 
    versionKey: false,
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
 }
)

const colourModel = mongoose.model('colours',colourSchema);

module.exports = colourModel;
