const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const userRouter = require("./router/user-router");
const categoryRouter = require("./router/ category-router");
const coloursRouter = require('./router/colours-router');
const productRouter = require('./router/product-router')
const brandRouter = require('./router/brand-router');
const reviewRouter = require('./router/review.router')
const ratingRouter = require('./router/rating.router')
const globleHandler = require("./controllers/error-handler");
const app = express();
// const client = redis.createClient({
//   host: '127.0.0.1',
//   port: 6379,
//   db: 0
// }); // this creates a new client


app.use(express.json({ limit: "10kb" }));
dotenv.config({ path: "./config.env" });

const DATABASE = process.env.DATABASE;


mongoose
  .connect(DATABASE, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  })
  .then(() => {
    // console.log('mongodb connected')
  });

// client.on('connect', function () {
//   // console.log('Redis client connected');
// });

// client.on('error', function (err) {
//   // console.log('Something went wrong ' + err);
// });

app.use("/api/v1/users/", userRouter);
app.use("/api/v1/categories/", categoryRouter);
app.use("/api/v1/colours/", coloursRouter);
app.use("/api/v1/products/", productRouter);
app.use("/api/v1/brands/", brandRouter);
app.use("/api/v1/reviews/", reviewRouter);
app.use("/api/v1/ratings/", ratingRouter);
app.use(globleHandler);

module.exports = app;
