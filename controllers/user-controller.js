const userModel = require('../models/user-model');
const appError = require('../utils/appError');
const catchAsync = require('../utils/catchAsync');
const apiFeature = require('../utils/api-feature');
const { use } = require('../app');
exports.getUsers = catchAsync(async (req, res, next) => {
    // console.log(req.query);
    const filter = {}
    const feature = new apiFeature(userModel.find(filter), req.query)
        .filter()
        .sort()
        .paginate()
    // console.log(feature)
    const docs = await feature.query;
    const totalCountQUery = await new apiFeature(userModel.find(filter), req.query)
        .filter()
        .sort();
    const totalCount = await totalCountQUery.query.countDocuments();
    res.status(200).send({
        msg: 'auth Controller',
        totalCount,
        docs
    })
})

exports.getUser = catchAsync(async (req, res, next) => {
  
    const doc = await userModel.findById(req.idNo);
    if(!doc){
        return next(new appError('user not found',400))
    }
    res.status(200).send({
        message:'success',
        doc
    })
})

exports.deleteuser = catchAsync(async(req,res,next)=>{
    // console.log(req.idNo);
    // const doc = await userModel.findByIdAndUpdate(req.idNo, {active:false}, {new:true});
    const doc = await userModel.findByIdAndUpdate(req.idNo, {active:false}, {new:true});
     if(!doc){
         return next(new appError('user not found',404))
     }
    //  console.log(doc)
     res.sendStatus(204).send({
         message:'success',
        //  doc
     })
})

exports.updateUser = catchAsync(async(req,res,next)=>{
    // console.log('req.body')
    const doc = await userModel.findByIdAndUpdate(req.idNo, req.body , {
        new :true,
        runValidators: true
    })

    res.status(200).send({
        message:'success',
        doc
    })
})

exports.updateAll = catchAsync(async(req,res,next)=>{
    const docs = await userModel.updateMany({role:'admin'},{$set:{ role: req.body.role }},{new:true, runValidators: true});
    res.status(200).send({
        message:'success',
        docs
    })
})


