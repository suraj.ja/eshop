const catchAsync = require('../utils/catchAsync');
const appError = require('../utils/appError');
const categoryModel = require('../models/category-model')
exports.createCategory = catchAsync(async(req,res,next)=>{
    const doc = await categoryModel.create({name:req.body.name});

    if(!doc){
        return next(new appError('category not created',400))
    }
    res.status(201).send({
        message:'succes',
        doc
    })
})

exports.getAllCategory = catchAsync(async(req,res,next)=>{
    const doc = await categoryModel.find({active:true}).select('-__v');
    res.status(201).send({
        message:'succes',
        doc
    })
});

exports.getCateory = catchAsync(async(req,res,next)=>{
    const doc = await categoryModel.find({_id:req.idNo , active:true}).select('-__v');
    if(!doc || doc.length <= 0 ){
        return next(new appError('no result found', 400 ))
    }
    res.status(200).send({
        message:'succes',
        doc
    })
})

exports.updateCateory = catchAsync(async(req,res,next)=>{
    // console.log(req.idNo)
    const doc = await categoryModel.findByIdAndUpdate(req.idNo, req.body , {
        new :true,
        runValidators: true
    })
    res.status(200).send({
        message:'success',
        doc
    })
})


exports.deleteCategory = catchAsync(async(req,res,next)=>{
    const doc = await categoryModel.findByIdAndUpdate(req.idNo, {active:false} , {
        new :true,
        runValidators: true
    })
    //    console.log(doc)
    res.status(204).send({
        message:'deleted successfully'
    })
});
