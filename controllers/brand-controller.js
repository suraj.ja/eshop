const brandModel = require('../models/brand-model')
const commonHandler = require('./commonHandler')

exports.createBrand = commonHandler.create(brandModel);

exports.getAllBrands = commonHandler.getAll(brandModel);

exports.getBrand = commonHandler.getOne(brandModel);

exports.updateBrand = commonHandler.updateOne(brandModel);

exports.deleteBrand = commonHandler.deleteOne(brandModel);
