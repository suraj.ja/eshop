const coloursModel = require('../models/colours-model')
const commonHandler = require('./commonHandler')

exports.createcolour = commonHandler.create(coloursModel);

exports.getAllColours = commonHandler.getAll(coloursModel);

exports.getColor = commonHandler.getOne(coloursModel);

exports.updateColour= commonHandler.updateOne(coloursModel);

exports.deleteColour = commonHandler.deleteOne(coloursModel);
