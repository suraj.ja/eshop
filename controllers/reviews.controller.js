const catchAsync = require('../utils/catchAsync');
const commoHandller = require('./commonHandler');
const reviewModel = require('../models/review-model');
const userModel = require('../models/user-model')
const APIFeatures = require("../utils/api-feature");


exports.getAllReviews = catchAsync(async(req,res,next)=>{
    // console.log(req.user);
    const filter = {active:true}
    const feature =  new APIFeatures(reviewModel.find(filter).populate({path:'product',select:'-createdAt'})).filter().sort().paginate();
    const doc = await feature.query;
    // const doc = await reviewModel.find().populate({ path: 'user' });
    res.status(200).send({
        message:'success',
        doc
    })
})

exports.getOneReview = catchAsync(async(req,res,next)=>{
    // console.log(req.idNo);
    const filter = {active:true,_id:req.idNo }
    const feature =  new APIFeatures(reviewModel.find(filter).populate({path:'product'})).filter().sort().paginate();
    const doc = await feature.query;
    // const doc = await reviewModel.find().populate({ path: 'user' });
    res.status(200).send({
        message:'success',
        doc
    })
})

exports.createReview = catchAsync(async(req,res,next)=>{
    // console.log(req.user);
    req.body.user = req.user.id
    const doc = await reviewModel.create(req.body)
    res.status(201).send({
        message:'success',
        doc
    })
})
// exports.createReview = commoHandller.create(reviewModel);