const catchAsync = require('../utils/catchAsync');
const commonHandler = require('../controllers/commonHandler');
const productModel = require('../models/products-model');
const appError = require("../utils/appError");
const APIFeatures = require("../utils/api-feature");

exports.getAllProduct = catchAsync(async (req, res, next) => {
    // console.log(req.query);
    var filter = {active: true}
    if(req.baseUrl.includes('/product')){
        if(req.query.catid){
            var catIds = req.query.catid.split(',');
            // console.log(catIds)
            filter.category = { "$in" : catIds} 
        }
        
        if(req.query.colours){
            const colours = req.query.colours.split(',')
            filter.colours = { "$in" : colours} 
        }
        if(req.query.brandid){
            filter.brand = req.query.brandid
        }
        if(req.query.price){
            filter.actulPrice = {$lte: req.query.price}
        }
    }
    const feature = new APIFeatures(productModel.find(filter).populate({path:'brand', select:'-createdAt'}).populate({path:'category', select:'-createdAt'}).populate({path:'colours', select:'-createdAt'})).filter().sort().paginate();
    const totalCountQuery = new APIFeatures(productModel.find(filter)).filter().sort();
    const doc = await feature.query
    const totalCount = await totalCountQuery.query.countDocuments();
    res.status(200).send({
      message: "success",
      totalCount,
      doc
    });
  });

exports.getProduct = catchAsync(async(req,res,next)=>{
    const feature = new APIFeatures(productModel.find({_id:req.idNo , active:true}).populate({path:'brand', select:'-createdAt'}).populate({path:'category', select:'-createdAt'}).populate({path:'colours', select:'-createdAt'}));
    
    const doc = await feature.query;
    if(!doc || doc.length <= 0 ){
        return next(new appError('no result found', 400 ))
    }
    res.status(200).send({
        message:'succes',
        doc
    })
})

exports.createProduct = catchAsync(async(req,res,next)=>{
    const data = req.body;
    // console.log(data.brand)
    if(!data.category || data.category.length <= 0){
        return next(new appError('category must be added', 400))
    }
    if(!data.brand){
        return next(new appError('brand must be added', 400))
    }
    const doc = await productModel.create(data);
    res.status(201).send({
        message:'success',
        doc
    })
})

exports.updateProduct = catchAsync(async(req,res,next)=>{
    const data = req.body;
    console.log(data.category)
    if(data.category){
        if(data.category.length <= 0)
        return next(new appError('category must be added', 400))
    }
    const doc = await productModel.findByIdAndUpdate(req.idNo, data, {runValidators:true,new:true})
    res.status(201).send({
        message:'success',
        doc
    })
})

exports.deleteProduct = commonHandler.deleteOne(productModel);