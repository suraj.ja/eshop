const catchAsync = require("../utils/catchAsync");
const appError = require("../utils/appError");
const APIFeatures = require("../utils/api-feature");

exports.getAll = (Model) =>
  catchAsync(async (req, res, next) => {
    // console.log(req.query);
    var filter = {active: true}
    if(req.baseUrl.includes('/product')){
        if(req.query.catid){
            var catIds = req.query.catid.split(',');
            // console.log(catIds)
            filter.category = { "$in" : catIds} 
        }
        
        if(req.query.colours){
            const colours = req.query.colours.split(',')
            filter.colours = { "$in" : colours} 
        }
        if(req.query.brandid){
            filter.brand = req.query.brandid
        }
        if(req.query.price){
            filter.actulPrice = {$lte: req.query.price}
        }
    }
    const feature = new APIFeatures(Model.find(filter).populate()).filter().sort().paginate();
    const totalCountQuery = new APIFeatures(Model.find(filter)).filter().sort();
    const doc = await feature.query
    const totalCount = await totalCountQuery.query.countDocuments();
    res.status(200).send({
      message: "success",
      totalCount,
      doc
    });
  });

exports.getOne = Model =>
catchAsync(async(req,res,next)=>{
    const feature = new APIFeatures(Model.find({_id:req.idNo , active:true}));
    const doc = await feature.query;
    if(!doc || doc.length <= 0 ){
        return next(new appError('no result found', 400 ))
    }
    res.status(200).send({
        message:'succes',
        doc
    })
})

exports.getOnetest = (Model, popOptions) =>
  catchAsync(async (req, res, next) => {
    let query = Model.findById(req.params.id);
    if (popOptions) query = query.populate(popOptions);
    const doc = await query;

    if (!doc) {
      return next(new AppError('No document found with that ID', 404));
    }

    res.status(200).json({
      status: 'success',
      data: {
        data: doc
      }
    });
  });
exports.updateOne = Model =>
catchAsync(async(req,res,next)=>{
    // console.log(req.idNo)
    const feature = new APIFeatures(Model.findByIdAndUpdate(req.idNo, req.body , {
        new :true,
        runValidators: true
    }));
    const doc = await feature.query
    res.status(200).send({
        message:'success',
        doc
    })
})

exports.deleteOne = Model =>
catchAsync(async(req,res,next)=>{
    const feature = new APIFeatures(Model.findByIdAndUpdate(req.idNo, {active:false} , {
        new :true,
        runValidators: true
    })) 
    const doc = await feature.query;
    res.status(204).send({
        message:'deleted successfully'
    })
});

exports.create = Model =>
catchAsync(async(req,res,next)=>{
    if(req.baseUrl.includes('reviews')){
        req.body.user = req.user._id
    }
    const feature = new APIFeatures(Model.create(req.body)) 
    const doc = await feature.query;
    if(!doc){
        return next(new appError('doc not created',400))
    }
    res.status(201).send({
        message:'succes',
        doc
    })
})



