const {promisify} = require('util')
const userModel = require('../models/user-model');
const catchAsync = require('../utils/catchAsync');
const appError = require('../utils/appError')
var jwt = require('jsonwebtoken');

exports.getId = catchAsync(async (req, res, next) => {
    // console.log('req')
    if (!req.params.id) {
        return next(new appError('id not specified', 400))
    }
    req.idNo = req.params.id.trim();
    next();
})

const createToken = (user)=>{
    const id = user._id;
    return jwt.sign({ id }, process.env.JWT_SECRET_KEY, { expiresIn: process.env.JWT_EXPIRES_IN });
}

const sendToken = async(user,statusCode,res)=>{
    const tokenVal = await createToken(user);
    res.cookie('token',tokenVal,{expire: process.env.TOKEN_EXPIRES_IN * 24 * 60 * 60 * 1000 });
    user.password = undefined;
    res.status(statusCode).send({
        token:tokenVal,
        message:'success',
        user
    })
}

exports.signup = catchAsync(async(req,res,next)=>{
    let obj = {};
    obj.name = req.body.name;
    obj.email = req.body.email;
    obj.password = req.body.password;
    obj.passwordConfirm = req.body.passwordConfirm;
    if(req.body.active !== undefined ||req.body.active !== null ){
        obj.active = req.body.active
    }
    if(req.body.role !== undefined ||req.body.role !== null ){
        obj.role = req.body.role
    }
    const user = await userModel.create(obj);
    sendToken(user,201,res);
})

exports.login = catchAsync(async(req,res,next)=>{
    const email = req.body.email;
    const password = req.body.password;
    const user = await userModel.findOne({email:email}).select('+password')
    // console.log(user);
    if(!user){
        return next(new appError('usr not found',400))
    }
    const isPassCorrect = await user.correctPassword(password,user.password);
    // console.log(isPassCorrect)
    if(!isPassCorrect){
        return next(new appError('invalid credentials',400))
    }
    sendToken(user,200,res);
});

exports.authentication = catchAsync(async(req,res,next)=>{
    const token = req.headers.token;
    // console.log('token')
    if(!token){
        return next(new appError('token not provided !! Authentication failed !!'))
    }
    const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET_KEY);
    const user = await userModel.findById(decoded.id)
    if(!user){
        return next(new appError('user not found'))
    }
    req.user = user;
    next();
})

exports.authorization = (...roles)=>{
   return (req,res,next)=>{
    //    console.log(roles, (req.user.role))
       if(roles.indexOf(req.user.role) < 0 ){
        return next(new appError('not authorized !!',400))
       }
       next()
   }
}