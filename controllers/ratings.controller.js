const ratingsModel = require('../models/ratings.model');
const catchAsync = require('../utils/catchAsync');
const appError = require('../utils/appError');
const APIFeatures = require('../utils/api-feature');


exports.createRating = catchAsync(async(req,res,next)=>{
    req.body.users = req.user.id
    // console.log(req.body)
    const doc = await ratingsModel.create(req.body)
    res.status(201).send({
        message:'success',
        doc
    })
})

exports.getAllratings = catchAsync(async(req,res,next)=>{
    const filter = {}
    const feature = new APIFeatures(ratingsModel.find(filter).populate({path:'products'}).populate({path:'users',select:'-createdAt'})).filter().sort().paginate();
    const doc = await feature.query;
    res.status(201).send({
        message:'success',
        doc
    })
})

exports.getOneRatings = catchAsync(async(req,res,next)=>{
    const filter = {_id:req.idNo }
    const feature = new APIFeatures(ratingsModel.find(filter).populate({path:'users'})).filter().sort().paginate();
    const doc = await feature.query;
    res.status(201).send({
        message:'success',
        doc
    })
})